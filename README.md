## Favorite Movies App

Application for collecting movies.

In this application, you can search for movies through the API - there is a search by movie title and year.
As a result of the search we get a list of films with the appropriate name and year.
Any movie from the list can be added to "favorite movies" by clicking on the "heart" icon.

You can also manually create a movie card — a form will appear when you click a plus button.
After filling in all the fields in the form and clicking on the "Create movie card" button, the created movie is added to the list of favorite movies.

Movies also can be removed from the list of favorite movies by clicking on the cross icon.


## Deployment

### `yarn install`


### `setup key`

Apikey located in the config.js file.
If apikey has expired, you can get it by doing the following:
   * Create account at https://www.themoviedb.org/
   * Follow instructions from https://developers.themoviedb.org/3/getting-started/introduction

### `yarn start`

## App stack
    React
    Redux
    Saga
    React-Router
    Axios
    Material UI
    Lodash
    