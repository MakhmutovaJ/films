import isEmpty from 'validator/lib/isEmpty';

export const validateCard = data => {
    const errors = {};

    if (isEmpty(data.movieName)) errors.movieName = 'Can`t be blank';
    if (isEmpty(data.language)) errors.language = 'Can`t be blank';

    return errors;
};

export const validateSearch = movieName => {
  const errors = {};
  if (isEmpty(movieName)) errors.movieName = 'Can`t be blank';
  return errors;
};