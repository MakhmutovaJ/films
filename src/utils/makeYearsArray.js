export const makeYearsArray = year => {
    const minOffset = 0;
    const maxOffset = 100;
    const options = [];

    for (let i = minOffset; i <= maxOffset; i++) {
        const newYear = year - i;
        options.push(newYear);
    }

    return options;
};