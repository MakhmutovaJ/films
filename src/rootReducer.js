import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import moviesReducer from './ducks/movies/reducer';

export default history => combineReducers({
    router: connectRouter(history),
    movies: moviesReducer,

});
