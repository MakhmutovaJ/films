import React, { Component } from 'react';
import CreateMovieCardComponent from '../../components/MovieCard/CreateMovieCardComponent';
import { connect } from 'react-redux';
import { addMovieCard } from '../../ducks/movies/reducer';
import { validateCard } from '../../utils/validation';
import { makeYearsArray } from '../../utils/makeYearsArray';

const ratingVariants = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const yearVariants = makeYearsArray(2019);

const initialState = {
    data: {
        movieName: '',
        year: 2019,
        language: '',
        rating: ratingVariants[9],
        isFavorite: true,
    },
    open: false,
    errors: {
        movieName: '',
        language: '',
    },
};

class CreateMovieCardContainer extends Component {
    constructor(props) {
        super(props);
        this.state= initialState;
    };

    handleChange = name => e => {
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                [name]: e.target.value,
            }
        });
    };

    handleClick = e => {
        e.preventDefault();
        const { data } = this.state;
        const errorValidation = validateCard(data);
        this.setState({
            errors: errorValidation
        });
        if(Object.values(errorValidation).length === 0) {
            this.props.addMovieCard(data);
            this.setState(initialState);
        }
    };

    handleOpen = e => {
        e.preventDefault();
        this.setState({
            open: true,
        })
    };

    handleClose = e => {
        e.preventDefault();
        this.setState({
            open: false,
        })
    };

    render() {
        const { data, open, errors } = this.state;

        return (
            <CreateMovieCardComponent
                onChange={this.handleChange}
                onClick={this.handleClick}
                onOpen={this.handleOpen}
                onClose={this.handleClose}
                data={data}
                open={open}
                errors={errors}
                ratingVariants={ratingVariants}
                yearVariants={yearVariants}
            />
        )
    }
};

const mapDispatchToProps = ({ addMovieCard });

export default connect(null, mapDispatchToProps)(CreateMovieCardContainer);