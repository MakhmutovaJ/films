import React, { Component }from 'react';
import { connect } from 'react-redux';
import { searchMovie, addToFavorite } from '../../ducks/movies/reducer';
import SearchComponent from "../../components/Search/SearchComponent";
import { selectMovies } from "../../ducks/movies/selectors";
import FavoriteMovieListComponent from "../../components/MovieList/FavoriteMovieListComponent";
import Favorite from '@material-ui/icons/Favorite';
import { makeYearsArray } from "../../utils/makeYearsArray";
import { validateSearch } from '../../utils/validation';
import LoaderComponent from "../../components/Loader/LoaderComponent";

const initialState = {
    data: {
        movieName: 'batman',
        year: '',
    },
    errors: {
        movieName: '',
    }
};

const yearVariants = makeYearsArray(2019);

class MovieListContainer extends Component {

    constructor(props) {
        super(props);
        this.state = initialState;
    };

    componentDidMount() {
        const { data } = this.state;
        const { movies, searchMovie } = this.props;
        if(movies.length === 0) {
            searchMovie(data);
        }
    };

    handleChange = name => e => {
        e.preventDefault();
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                [name]: e.target.value,
            },
        });
    };

    handleSearchMovie = e => {
        e.preventDefault();
        const { movieName } = this.state.data;
        const errorValidate = validateSearch(movieName);
        this.setState({
            errors: errorValidate,
        });
        if (Object.values(errorValidate).length === 0) {
            const { data } = this.state;
            this.props.searchMovie(data);
        }
    };

    handleAddToFavorite = data => e => {
        e.preventDefault();
        this.props.addToFavorite(data);
    };

    handleClearFilters = e => {
        e.preventDefault();
        this.setState({
            ...this.state,
            data: {
                movieName: '',
                year: '',
            },
        });
    };

    handleClearName = e => {
        e.preventDefault();
        this.setState({
            ...this.state,
            data: {
                ...this.state.data,
                movieName: '',
            },
        });
    };

    render() {
        const { movies, isLoader } = this.props;
        const { data: { movieName, year }, errors } = this.state;

        return (
            <>
                <SearchComponent
                    onChange={this.handleChange}
                    onClearFilters={this.handleClearFilters}
                    onClearName={this.handleClearName}
                    onClick={this.handleSearchMovie}
                    movieName={movieName}
                    year={year}
                    yearVariants={yearVariants}
                    errors={errors}
                />
                {isLoader ? <LoaderComponent/> : <FavoriteMovieListComponent
                    movies={movies}
                    tooltipText='Add to favorites'
                    propsIcon={<Favorite/>}
                    onClickFavorite={this.handleAddToFavorite}
                />}

            </>
        )
    };

};

const mapDispatchToProps = ({ searchMovie, addToFavorite });

const mapStateToProps = state => {
    return {
        movies: selectMovies(state),
        isLoader: state.movies.isLoader,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieListContainer);