import React, { Component } from 'react';
import FavoriteMovieListComponent from '../../components/MovieList/FavoriteMovieListComponent';
import { connect } from 'react-redux';
import { selectFavoriteMovies } from "../../ducks/movies/selectors";
import { deleteMovieCard } from '../../ducks/movies/reducer';
import Clear from '@material-ui/icons/Clear';


class FavoriteMovieListContainer extends Component {
    handleDelete = data => e => {
        e.preventDefault();
        this.props.deleteMovieCard(data.id);
    };

    render() {
        const { movies } = this.props;
        return (
            <FavoriteMovieListComponent
                movies={movies}
                tooltipText='Remove from favorites'
                propsIcon={<Clear color='error' />}
                onClickFavorite={this.handleDelete}
            />
        );
    };
};

const mapDispatchToProps = ({ deleteMovieCard });

const mapStateToProps = state => {
    return {
        movies: selectFavoriteMovies(state),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteMovieListContainer);