import React, { Fragment} from 'react';
import { Route } from 'react-router-dom';
import MovieListContainer from '../MovieList/MovieListContainer';
import FavoriteMovieListContainer from '../MovieList/FavoriteMovieListContainer';
import HeaderComponent from '../../components/Header/HeaderComponent'
import CreateMovieCardContainer from "../MovieCard/CreateMovieCardContainer";

export const App = () => (
    <Fragment>
        <HeaderComponent />
        <Fragment>
            <Route exact path="/" component={MovieListContainer} />
            <Route exact path="/favorite-movie-list" component={FavoriteMovieListContainer} />
        </Fragment>
        <CreateMovieCardContainer />
    </Fragment>
);