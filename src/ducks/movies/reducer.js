import { createReducer, createAction } from 'redux-act';
import { uniqueId, omit } from 'lodash';

const initialState = { movies: {}, favoriteMovies: {} };

export const addMovieCard = createAction('@MOVIES/ADD_MOVIE_CARD');
export const deleteMovieCard = createAction('@MOVIES/DELETE_MOVIE_CARD');

export const searchMovie = createAction('@MOVIES/SEARCH_MOVIE');
export const searchMovieSuccess = createAction('@MOVIES/SEARCH_MOVIE_SUCCESS');
export const searchMovieFailure = createAction('@MOVIES/SEARCH_MOVIE_FAILURE');

export const addToFavorite = createAction('@MOVIES/ADD_TO_FAVORITE');

const handleAddMovieCard = (state, data) => {
    const id = uniqueId();
    const newData = {
        ...data,
        id,
    };
    return {
      ...state,
      favoriteMovies: {
        ...state.favoriteMovies,
        [id]: newData,
      },
  };

};

const handleDeleteMovieCard = (state, id) => {
    const favoriteMovies = omit(state.favoriteMovies, id);
    const movies = {
        ...state.movies,
        [id]: {
            ...state.movies[id],
            isFavorite: false,
        }
    };
    return {
        ...state,
        favoriteMovies,
        movies,
    };
};

const handleSearchMovie = (state) => {
  return {
      ...state,
      isLoader: true,
  }
};

const handleSearchMovieSuccess = (state, data) => {
    return {
        ...state,
        movies: data,
        isLoader: false,
    }
};

const handleSearchMovieFailure = (state, error) => {
    // TODO handler failures
};

const handleAddToFavorite = (state, data) => {
    const favoriteMovies = {
        ...state.favoriteMovies,
        [data.id]: {
            ...data,
            isFavorite: true,
        },
    };
    const movies = {
        ...state.movies,
        [data.id]: {
            ...data,
            isFavorite: true,
        },
    };
    return {
        ...state,
        favoriteMovies,
        movies,
    };
};

const reducer = createReducer((on) => {
    on(addMovieCard, handleAddMovieCard);
    on(deleteMovieCard, handleDeleteMovieCard);
    on(searchMovie, handleSearchMovie);
    on(searchMovieSuccess, handleSearchMovieSuccess);
    on(searchMovieFailure, handleSearchMovieFailure);
    on(addToFavorite, handleAddToFavorite);
}, initialState);

export default reducer;
