import { takeLatest, all, put } from 'redux-saga/effects';
import { searchMovie, searchMovieSuccess, searchMovieFailure } from "./reducer";
import axios from 'axios';
import { moviesListMapper } from './mapper';
import apikey from '../../config';

function* searchMovieSaga({payload}) {
    const { movieName, year } = payload;
    const { apiKey } = apikey;
    try {
        const respons = yield axios.get(
            `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${movieName}&year=${year}`
        );
        const data = moviesListMapper(respons);
        yield put(searchMovieSuccess(data));

    } catch(error) {
        yield put(searchMovieFailure);
    }
};

export default function* searchSagas() {
    yield all([
        takeLatest(searchMovie, searchMovieSaga)
    ]);
}



