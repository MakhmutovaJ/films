export const selectMovies = state => {
    return Object.values(state.movies.movies);
};

export const selectFavoriteMovies = state => {
    return Object.values(state.movies.favoriteMovies);
};
