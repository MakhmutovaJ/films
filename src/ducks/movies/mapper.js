import { get } from 'lodash';

const transformData = item => {
    return {
        id: get(item, 'id', ''),
        movieName: get(item, 'title', ''),
        year: get(item, 'release_date', ''),
        language: get(item, 'original_language', ''),
        rating: get(item, 'vote_average',''),
        isFavorite: false,
    }
};

export const moviesListMapper = data => {
    const newData = data.data.results.reduce((acc, item) => {
        acc[item.id] = transformData(item);
        return acc;
    },{});

    return newData;
};