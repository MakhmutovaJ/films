import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import createHistory from 'history/createBrowserHistory'
import createRootReducer from "./rootReducer";
import movieSaga from "./ducks/movies/sagas";
import createSagaMiddleware from "redux-saga";

export const history = createHistory();
const sagaMiddleware = createSagaMiddleware();

const initialState = {};
const rootReducer = createRootReducer(history);
const enhancers = [];
const middleware = [
    routerMiddleware(history),
    sagaMiddleware,
];

if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__

    if (typeof devToolsExtension === 'function') {
        enhancers.push(devToolsExtension())
    }
};

const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
);


const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
);

sagaMiddleware.run(movieSaga);

export default store;