import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    loader: {
        position: 'absolute',
        top: '60%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        zIndex: 1002,
    },
}));


const LoaderComponent = () => {
    const classes = useStyles();

    return (
       <CircularProgress className={classes.loader} />
    )
};

export default LoaderComponent;