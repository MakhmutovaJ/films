import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Clear from '@material-ui/icons/Clear';
import MenuItem from "@material-ui/core/MenuItem";
import InlineErrorComponent from "../InlineErrors/InlineErrorComponent";
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';

const useStyles = makeStyles(theme => ({
    filter: {
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        maxWidth: 900,
        margin: '0 auto',
    },
    search: {
        display: 'flex',
        flexGrow: 3,
        margin: '30px 10px 10px 20px',
    },
    year: {
        margin: '30px 10px 10px 10px',
        flexGrow: 2,
    },
    error: {
        paddingLeft: 20,
    },
    searchButton: {
        margin: '30px 10px 30px 10px',
        flexGrow: 0.5,
    },
    clearButton: {
        margin: '30px 50px 30px 10px',
        flexGrow: 0.5,
        backgroundColor: '#ef5350',
        color: 'white',
        '&:hover': { backgroundColor: '#e53935' },
    },
}));

const SearchComponent = ({
    onChange,
    onClearFilters,
    onClearName,
    onClick,
    movieName,
    year,
    yearVariants,
    errors
}) => {
    const classes = useStyles();

    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    return (
        <div className={classes.filter}>
            <FormControl margin='none'  className={classes.search}>
                <TextField
                    autoFocus
                    value={movieName}
                    variant="outlined"
                    placeholder='Searching Movie'
                    onChange={onChange('movieName')}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={onClearName}>
                                    <Clear />
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                <InlineErrorComponent errorText={errors.movieName} className={classes.error}/>
            </FormControl>
            <FormControl variant="outlined" className={classes.year}>
                <InputLabel ref={inputLabel} htmlFor="outlined-year">
                    Year
                </InputLabel>
                <Select
                    value={year}
                    onChange={onChange('year')}
                    input={<OutlinedInput  labelWidth={labelWidth} id="outlined-year" />}
                >
                    {yearVariants.map(option => (
                        <MenuItem value={option} >
                            {option}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <Button
                color='primary'
                variant="contained"
                className={classes.searchButton}
                onClick={onClick}
            >
                Search
            </Button>
            <Button
                onClick={onClearFilters}
                variant="contained"
                className={classes.clearButton}
            >
                Clear
            </Button>
        </div>
    );
};

export default SearchComponent;