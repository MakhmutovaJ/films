import React from 'react';
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    style: {
        color: '#bdbdbd',
        margin: '100px auto',
    },
}));

const NotFoundComponent = () => {

    const classes = useStyles();

    return (
        <Typography variant='h3' align='center' className={classes.style}>
            Nothing was found
        </Typography>
    );
};

export default  NotFoundComponent;
