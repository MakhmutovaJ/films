import React from 'react';
import FormHelperText from '@material-ui/core/FormHelperText';

const errorStyle = { color: 'red' };

const InlineErrorComponent = ({errorText}) => {
    return (
        <FormHelperText style={errorStyle}>{errorText}</FormHelperText>
    )
};

export default InlineErrorComponent;