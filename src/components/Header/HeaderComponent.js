import React from "react";
import MenuComponent from "../Menu/MenuComponent";
import { makeStyles } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(theme => ({
    paperStyle:{
        padding: 20,
    },
}));

const HeaderComponent = () => {
    const classes = useStyles();

    return (
        <Paper className={classes.paperStyle}>
            <MenuComponent />
        </Paper>
    );
};

export default HeaderComponent;