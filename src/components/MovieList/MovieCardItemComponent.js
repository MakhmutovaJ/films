import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from "@material-ui/core";
import IconButton from '@material-ui/core/IconButton';
import Typography from "@material-ui/core/Typography";
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles(theme => ({
    card: {
        borderRadius: 15,
        margin: '20px 20px',
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        width: 250,
    },
    icon: {
        alignSelf: 'flex-end',
        padding: 0,
        '&:hover': { color: '#e53935' },
    },
    typography: {
        paddingTop: 10,
        margin: 10,
        width: 100,
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
    },
    isFavorite: {
        color: '#e53935',
    },
    line: {
      display: 'flex',
    },
    field: {
        paddingTop: 10,
        margin: 10,
    },
}));


const MovieCardItemComponent = ({ data, propsIcon, onClickFavorite, flag, tooltipText }) => {

    const { movieName, year, language, rating } = data;

    const classes = useStyles();

    return (
        <Card className={classes.card}>
            <CardContent className={classes.item}>
                <IconButton
                    className={classes.icon}
                    onClick={onClickFavorite(data)}
                >
                    <Tooltip title={tooltipText} placement="right-start" className={flag && classes.isFavorite}>
                        {propsIcon}
                    </Tooltip>
                </IconButton>
                <div className={classes.line}>
                    <Typography color='primary' className={classes.field}> Name: </Typography>
                    <Typography color='secondary' className={classes.typography}> {movieName} </Typography>
                </div>
                <div className={classes.line}>
                    <Typography color='primary' className={classes.field}> Year: </Typography>
                    <Typography color='secondary' className={classes.typography}> {year} </Typography>
                </div>
                <div className={classes.line}>
                    <Typography color='primary' className={classes.field}> Language: </Typography>
                    <Typography color='secondary' className={classes.typography}> {language} </Typography>
                </div>
                <div className={classes.line}>
                    <Typography color='primary' className={classes.field}> Rating: </Typography>
                    <Typography color='secondary' className={classes.typography}> {rating}  </Typography>
                </div>
            </CardContent>
        </Card>
    );
};

export default MovieCardItemComponent;