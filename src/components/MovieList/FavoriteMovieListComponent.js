import React from 'react';
import MovieCardItemComponent from "./MovieCardItemComponent";
import { makeStyles } from "@material-ui/core";
import NotFoundComponent from "../InlineErrors/NotFoundComponent";

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'center',
        margin: '0 auto',
        maxWidth: 900,
    },
}));

const FavoriteMovieListComponent = ({ movies, propsIcon, tooltipText, onClickFavorite }) => {

    const classes = useStyles();

    return (
        <div className={classes.card}>
            {(movies.length === 0) && <NotFoundComponent />}
            {movies.map(item => (
                <MovieCardItemComponent
                    data={item}
                    propsIcon={propsIcon}
                    flag={item.isFavorite}
                    tooltipText={tooltipText}
                    onClickFavorite={onClickFavorite}
                />
            ))}
        </div>
    );
};

export default FavoriteMovieListComponent;