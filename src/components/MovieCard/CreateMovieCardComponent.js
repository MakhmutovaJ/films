import React from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from "@material-ui/core/Typography";
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import AddCircleOutline from '@material-ui/icons/AddCircleOutline';
import InlineErrorComponent from "../InlineErrors/InlineErrorComponent";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Clear from '@material-ui/icons/Clear';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(4),
        outline: 'none',
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        flexDirection: 'column',
        borderRadius: 15,
    },
    typography: {
        margin: '10px 0px',
    },
    iconButton: {
        position: 'fixed',
        bottom: 20,
        right: 20,
    },
    submit: {
        borderRadius: 10,
        marginTop: 30,
    },
    icon: {
        alignSelf: 'flex-end',
        padding: 0,
    },
}));

const CreateMovieCardComponent = ({
  onChange,
  onClick,
  onOpen,
  onClose,
  data,
  open,
  errors,
  ratingVariants,
  yearVariants
}) => {

    const classes = useStyles();

    const { movieName, year, language, rating } = data;

    return (
        <div>
            <IconButton
                className={classes.iconButton}
                onClick={onOpen}
                color='primary'
            >
                <AddCircleOutline fontSize="large" />
            </IconButton>
            <Modal
                open={open}
                onClose={onClose}
                className={classes.modal}
            >
                <div className={classes.paper}>
                    <IconButton className={classes.icon} onClick={onClose}>
                        <Clear color='primary' />
                    </IconButton>
                    <Typography
                        component='h1'
                        variant='h5'
                        align='center'
                    >
                        Add Movie
                    </Typography>
                    <FormControl margin='none'  fullWidth>
                        <TextField
                            autoFocus
                            label='Name'
                            value={movieName}
                            onChange={onChange('movieName')}
                            placeholder='Enter movie name'
                            type='text'
                        />
                        <InlineErrorComponent errorText={errors.movieName} />
                    </FormControl>
                    <FormControl margin='none' fullWidth>
                        <TextField
                            label='Language'
                            value={language}
                            onChange={onChange('language')}
                            placeholder='Enter movie language'
                            type='text'
                        />
                        <InlineErrorComponent errorText={errors.language} />
                    </FormControl>
                    <FormControl margin='none' fullWidth className={classes.typography}>
                        <TextField
                            label='Year'
                            select
                            value={year}
                            onChange={onChange('year')}
                        >
                            {yearVariants.map(option => (
                                <MenuItem value={option} >
                                    {option}
                                </MenuItem>
                            ))}
                        </TextField>
                    </FormControl>
                    <FormControl margin='none' fullWidth className={classes.typography}>
                        <TextField
                            label='Rating'
                            select
                            value={rating}
                            onChange={onChange('rating')}
                        >
                            {ratingVariants.map(option => (
                                <MenuItem value={option} >
                                    {option}
                                </MenuItem>
                            ))}
                        </TextField>
                    </FormControl>
                    <Button
                        onClick={onClick}
                        type='submit'
                        variant='contained'
                        color='primary'
                        className={classes.submit}
                    >
                        Create Movie Card
                    </Button>
                </div>
            </Modal>
        </div>
    );
};

export default CreateMovieCardComponent;