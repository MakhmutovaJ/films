import React from "react";
import { Link } from "react-router-dom";
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import { ListItemText } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    menuList: {
        display: 'flex',
        justifyContent: 'center',
    },
    link: {
        textDecoration: 'none',
        color: '#2196f3',
    },
}));


const  MenuComponent = () => {
    const classes = useStyles();
    return (
        <MenuList className={classes.menuList}>
            <MenuItem>
                <Link to="/" className={classes.link}>
                    <ListItemText primary='Movies' />
                </Link>
            </MenuItem>
            <MenuItem>
                <Link to="/favorite-movie-list" className={classes.link}>
                    <ListItemText primary='Favorite Movies' />
                </Link>
            </MenuItem>
        </MenuList>
    );
};

export default MenuComponent;

