import React, { Fragment } from 'react';
import { render } from 'react-dom';
import { ConnectedRouter} from 'connected-react-router';
import store, { history } from './store';
import CssBaseline from '@material-ui/core/CssBaseline';
import './style.css';
import { Provider } from 'react-redux';
import { App } from './containers/App/App';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import grey from '@material-ui/core/colors/grey';

const appElement = document.getElementById('root');

const theme = createMuiTheme({ palette: { primary: blue, secondary: grey,} });

render (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <MuiThemeProvider theme={theme}>
                <Fragment>
                    <CssBaseline />
                    <App />
                </Fragment>
            </MuiThemeProvider>
        </ConnectedRouter>
    </Provider>,
    appElement
);


